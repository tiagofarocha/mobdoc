import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from './store';
import RouterSwitch from './routes/RouterSwitch';

ReactDOM.render(
    <Provider store={store}>
        <RouterSwitch />
    </Provider>, document.getElementById('root'));
