import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import MovieDetail from './MovieDetail';
import Movies from './Movies';
import CharacterDetail from './CharacterDetail';
import './styles/main.css'

export default class RouterSwitch extends Component {

	render() {
		return (
			<Router>
				<div className="container-app">
					<Route exact path="/" component={Movies} />
					<Route exact path="/movie-detail" component={MovieDetail} />
					<Route exact path="/character-detail" component={CharacterDetail} />
				</div>
			</Router>
		)
	}
}