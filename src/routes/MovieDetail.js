import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Actions from '../store/actions/characterActions';
import moment from 'moment';
import './styles/main.css'

class MovieDetail extends React.Component {

    componentDidMount() {
        let { movieSelected } = this.props
        if (movieSelected.data.characters) {
            movieSelected.data.characters.forEach((item) => {
                this.props.requestCharacterList(item)
            })
        }
    }

    handleSelectCharacter(character) {
        this.props.selectedCharacter(character)
        this.props.history.push("/character-detail")
    }

    render() {
        let { movieSelected, character } = this.props;
        if (!movieSelected || !character) {
            return null
        }
        return (
            <div className="container-movies">
                <div className="details">
                    <h2 className="title">Details of movie {movieSelected.data.title}</h2>
                    <p>Director: {movieSelected.data.director}</p>
                    <p>Created in: {moment(movieSelected.data.created).format('DD/MM/YYYY')}</p>
                    <p className="description">Description: {movieSelected.data.opening_crawl}</p>
                </div>
                <div className="characters">
                    <h2 className="title">Characters</h2>
                    {
                        character.data.map((item, index) => (
                            <div key={index} className="card">
                                <p 
                                    className="pointer" 
                                    onClick={() => this.handleSelectCharacter(item)}>{item.name}</p>
                            </div>
                        ))
                    }
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    movies: state.movies,
    movieSelected: state.movieSelected,
    character: state.character
});

const mapDispatchToProps = dispatch =>
    bindActionCreators(Actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(MovieDetail);
