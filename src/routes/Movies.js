import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Actions from '../store/actions/movieActions';

class Movies extends React.Component {
    constructor() {
        super()
    }

    componentDidMount() {
        this.props.requestMovies()
    }

    handleSelectMovie(movie) {
        this.props.selectedMovie(movie)
        this.props.history.push("/movie-detail")
    }

    render() {
        const { movies } = this.props

        if (!movies.data.results) {
            return null
        }

        return (
            <div className="container-movies">
                <h2 className="title">Movies of Star Wars</h2>
                {
                    movies.data.results.map((item, index) => (
                        <div key={index} className="card">
                            <h2>{item.title}</h2>
                            <p 
                                className="pointer"
                                onClick={() => this.handleSelectMovie(item)}>Details</p>
                        </div>
                    ))
                }
            </div>
        )
    }
}

const mapStateToProps = state => ({
    movies: state.movies,
});

const mapDispatchToProps = dispatch =>
    bindActionCreators(Actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Movies);
