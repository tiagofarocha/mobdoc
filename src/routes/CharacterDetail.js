import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Actions from '../store/actions/characterActions';
import './styles/main.css'

class CharacterDetail extends React.Component {

    componentDidMount() {
        let { characterSelected } = this.props
        if (characterSelected) {
            this.props.requestCharacterPlanet(characterSelected.data.homeworld)
        }
    }

    render() {
        let { characterSelected, characterPlanet } = this.props;
        return (
            <div className="container-movies">
                <div className="">
                    <div className="card">
                        <h2 className="title">Details of character {characterSelected.data.name}</h2>
                        <p>Body Height: {characterSelected.data.height}</p>
                        <p>Body Mass: {characterSelected.data.mass}</p>
                        <p>Homeworld: {characterPlanet.data.name}</p>
                        <p>Homeworld population: {characterPlanet.data.population}</p>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    characterSelected: state.characterSelected,
    characterPlanet: state.characterPlanet
});

const mapDispatchToProps = dispatch =>
    bindActionCreators(Actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(CharacterDetail);
