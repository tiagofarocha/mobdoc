export function requestCharacterList(character) {
    return {
      type: 'REQUEST_CHARACTER',
      payload: {
        data: character
      }
    }
}

export function selectedCharacter(character) {
  return {
    type: 'CHARACTER_SELECTED',
    payload: {
      data: character
    }
  }
}

export function requestCharacterPlanet(planet) {
  return {
    type: 'REQUEST_CHARACTER_PLANET',
    payload: {
      data: planet
    }
  }
}