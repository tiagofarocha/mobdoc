export function requestMovies() {
  return {
    type: 'REQUEST_MOVIES'
  }
}

export function selectedMovie(movie) {
  return {
    type: 'SELECTED_MOVIE',
    payload: {
      data: movie
    }
  }
}
