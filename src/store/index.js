import { createStore, combineReducers, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import movies from './reducers/moviesReducer';
import movieSelected from './reducers/selectedMovieReducer';
import character from './reducers/characterReducer';
import characterSelected from './reducers/selectedCharacterReducer';
import characterPlanet from './reducers/characterPlanetReducer';
import rootSaga from './sagas';

const sagaMiddleware = createSagaMiddleware();

const store = createStore(
  combineReducers({
    movies,
    movieSelected,
    character,
    characterSelected,
    characterPlanet
  }),
  applyMiddleware(sagaMiddleware),
);

sagaMiddleware.run(rootSaga);

export default store;
