import { delay } from 'redux-saga';
import { takeEvery, put, call, select } from 'redux-saga/effects';

async function apiMovies() {
  const responseMovies = await fetch(`https://swapi.co/api/films/`);
  const result = await responseMovies.json();
  return result
}

async function apiCharacters(character) {
  const responseCharacters = await fetch(character);
  const result = await responseCharacters.json();
  return result
}

async function apiCharacterPlanet(planet) {
  const responseCharacterPlanet = await fetch(planet);
  const result = await responseCharacterPlanet.json();
  return result
}

function* getMovies() {
  const responseMovies = yield call(apiMovies);
  yield put({ type: 'SUCCESS_MOVIES', payload: { data: responseMovies } });
}

function* getCharacters(action) {
  const responseCharacters = yield call(apiCharacters, action.payload.data);
  yield put({ type: 'SUCCESS_CHARACTER', payload: { data: responseCharacters } });
}

function* getCharacterPlanet(action) {
  const responseCharacterPlanet = yield call(apiCharacterPlanet, action.payload.data);
  yield put({ type: 'SUCESS_REQUEST_CHARACTER_PLANET', payload: { data: responseCharacterPlanet } });
}

export default function* root() {
  yield [
    takeEvery('REQUEST_MOVIES', getMovies),
    takeEvery('REQUEST_CHARACTER', getCharacters),
    takeEvery('REQUEST_CHARACTER_PLANET', getCharacterPlanet)
  ];
}
