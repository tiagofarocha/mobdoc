const INITIAL_STATE = {
  data: []
};

export default function movies(state = INITIAL_STATE, action) {
  switch (action.type) {
    case 'REQUEST_MOVIES':
      return { ...state };
    case 'SUCCESS_MOVIES':
      return { data: action.payload.data};
    default:
      return state;
  }
}
