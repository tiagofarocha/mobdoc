const INITIAL_STATE = {
    data: []
};

export default function movieSelected(state = INITIAL_STATE, action) {
    switch (action.type) {
      case 'SELECTED_MOVIE':
        return { data: action.payload.data};
      default:
        return state;
    }
  }