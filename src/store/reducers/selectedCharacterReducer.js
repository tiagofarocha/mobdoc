const INITIAL_STATE = {
    data: []
};

export default function selectedCharacter(state = INITIAL_STATE, action) {
    switch (action.type) {
      case 'CHARACTER_SELECTED':
        return { data: action.payload.data};
      default:
        return state;
    }
  }