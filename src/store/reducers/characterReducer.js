const INITIAL_STATE = {
    data: []
  };
  
  export default function character(state = INITIAL_STATE, action) {
    switch (action.type) {
      case 'REQUEST_CHARACTER':
        return { ...state };
      case 'SUCCESS_CHARACTER':
        return { data: [...state.data, action.payload.data] };
      default:
        return state;
    }
  }  